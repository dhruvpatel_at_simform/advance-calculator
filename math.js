//custom function for finding factorial
window.fact = function (num) {
  let f = 1;
  let isNag = false;
  num = parseInt(num);
  if (num < 0) {
    isNag = true;
    num = Math.abs(num);
  }
  for (let i = 1; i <= num; i++) {
    f *= i;
  }
  return isNag ? -f : f;
};

//custom function for finding 1/x or inverse
window.inverse = function (num) {
  return num === 0 ? Infinity : 1 / num;
};

const degToRad = (deg = 0) => (deg * Math.PI) / 180;
const radToDeg = (rad = 0) => (rad * 180) / Math.PI;

//main method for evaluating and returning result of expression
const evaluateArithmetiExp = (expressionString) => {
  // console.log(expressionString);
  try {
    // console.log(expressionString);
    const result = convertToExp(expressionString)();
    if (result === Infinity || result === -Infinity) {
      throw Error("evaluation is failed, please put valid values");
    }
    if (result === NaN) {
      throw Error("you had passed wrong values to functions, please correct.");
    }
    return Number.isInteger(result) ? result : parseFloat(result.toFixed(6));
  } catch (error) {
    if (error) {
      return (
        "Unable to Evaluate Expression, Please insert valid Expression " +
        " ," +
        error.message
      );
    }
  }
};

const convertToExp = (expressionString) => {
  return new Function(`return ${expressionString}`);
};

export { evaluateArithmetiExp, degToRad, radToDeg };
