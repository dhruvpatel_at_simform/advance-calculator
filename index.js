import * as Calc from "./math.js";

//global veriables
let expression = "0";
const bracketInfo = {
  isBracketsOpen: false,
  openingCounter: 0,
};
let dotPointer = null;
let currentDecimalInitIndexes = {
  expIndx: 0,
  dspexpIndx: 0,
};

// selecting require functions

const displayText = document.querySelector("#expression");
const numbers = document.querySelectorAll(".key.num");
const clearButton = document.querySelector("#clear");
const deleteButton = document.querySelector("#delete");
const openingBracketButton = document.querySelector("#opening-bracket");
const closingBracketButton = document.querySelector("#closing-bracket");
const additionButton = document.querySelector("#addition-operator");
const subtractButton = document.querySelector("#subtraction-operator");
const multiplyButton = document.querySelector("#multiplication-operator");
const devisionButton = document.querySelector("#devision-operator");
const moduloButton = document.querySelector("#modulo-operator");
const decimalDotButton = document.querySelector("#decimal-dot");
const evaluateButton = document.querySelector("#evaluate-exp");
const resultDisplay = document.querySelector("#result-display");
const expoButton = document.querySelector("#expo-operator");
const squareButton = document.querySelector("#square-operator");
const tensExpoButton = document.querySelector("#tens-expo");
const sqrtButton = document.querySelector("#sqrt-operator");
const factorialButton = document.querySelector("#factorial-operator");
const inverseButton = document.querySelector("#inverse-operator");
const logTenButton = document.querySelector("#log-ten");
const naturalLogButton = document.querySelector("#natural-log");
const sinButton = document.querySelector("#sin-function");
const cosButton = document.querySelector("#cos-function");
const tanButton = document.querySelector("#tan-function");
const degToRadInput = document.querySelector("#degree");
const radToDegInput = document.querySelector("#radian");
// console.log(openingBracket, closingBracket);

// helping methods
const isPreviousCharOperator = () => {
  return expression.match(/.*(\+|\-|\*|\/|\%)$/) !== null;
};
// callback fucntions for event listner
const isDotCanInstert = () => {
  return (
    expression.slice(dotPointer + 1, -1).match(/(\+|\-|\*|\/|\%|\(|\))/) !==
    null
  );
};
function onNumberClicked(num) {
  if (expression[0] === "0" && expression.length === 1) {
    if (num === "0") {
      return;
    } else {
      expression = num;
      displayText.innerText = num;
      return;
    }
  }
  if (
    expression[expression.length - 1] === ")" ||
    expression[expression.length - 1] === "!"
  ) {
    expression += `*${num}`;
    displayText.innerText += `*${num}`;
    return;
  }
  expression += num;
  displayText.innerText += num;
  return;
}

// adding event listner

// adding event listner to all numer and point(.)
numbers.forEach((number, i) =>
  number.addEventListener("click", () => onNumberClicked(number.innerText))
);

// eventlistner for decimal-dot
decimalDotButton.addEventListener("click", () => {
  if (!expression.includes(".")) {
    expression += ".";
    dotPointer = expression.length - 1;
    displayText.innerText += ".";
    return;
  } else {
    // console.log(isDotCanInstert());
    if (isDotCanInstert()) {
      expression += ".";
      dotPointer = expression.length - 1;
      displayText.innerText += ".";
    }
    return;
  }
});

//clear all input
const resetApp = () => {
  expression = "0";
  bracketInfo.openingCounter = 0;
  bracketInfo.isBracketsOpen = false;
  dotPointer = null;
  displayText.innerText = "0";
};

clearButton.addEventListener("click", () => resetApp());

// delete one number at click, backspace
deleteButton.addEventListener("click", () => {
  if (expression.length === 1) {
    resetApp();
    return;
  } else if (expression[expression.length - 1] === ".") {
    expression = expression.slice(0, -1);
    dotPointer = expression.lastIndexOf(".");
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  } else if (expression[expression.length - 1] === "(") {
    expression = expression.slice(0, -1);
    bracketInfo.openingCounter -= 1;
    if (bracketInfo.openingCounter === 0) bracketInfo.isBracketsOpen = false;
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  } else {
    expression = expression.slice(0, -1);
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  }
});

//event for paranthesis

const openingBracketInfoHandler = () => {
  if (bracketInfo.isBracketsOpen) {
    bracketInfo.openingCounter += 1;
  } else {
    bracketInfo.isBracketsOpen = true;
    bracketInfo.openingCounter = 1;
  }
};

openingBracketButton.addEventListener("click", () => {
  openingBracketInfoHandler();
  if (displayText.innerText === "0") {
    expression = "(";
    displayText.innerText = expression;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  }
  if (
    (expression[expression.length - 1] >= "0" &&
      expression[expression.length - 1] <= "9") ||
    expression[expression.length - 1] === ")"
  ) {
    // console.log("inside a open para digit");
    expression += "*(";
    displayText.innerText += "*(";
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  }
  // console.log("expression", expression);
  expression += "(";
  displayText.innerText += "(";
  currentDecimalInitIndexes.expIndx = expression.length;
  currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  return;
});

closingBracketButton.addEventListener("click", () => {
  if (
    bracketInfo.isBracketsOpen &&
    expression.charAt(expression.length - 1) !== "("
  ) {
    bracketInfo.openingCounter > 0
      ? (bracketInfo.openingCounter -= 1)
      : (bracketInfo.openingCounter = 0);
    if (bracketInfo.openingCounter === 0) {
      bracketInfo.isBracketsOpen = false;
    }
    expression += ")";
    displayText.innerText += ")";
    return;
  } else {
    return;
  }
});

// basic arithmetic operator listner
const isAvoidable = {
  "(": true,
  ".": true,
};

const arithmaticFunctionCallBack = (expString, displayExpString) => {
  openingBracketInfoHandler();
  const isAlreadyInserted = checkAndInsert(expString, displayExpString);
  if (isAlreadyInserted === "changed") {
    currentDecimalInitIndexes.expIndx += 2;
    currentDecimalInitIndexes.dspexpIndx += 2;
  }
  if (!isAlreadyInserted) {
    expression += expString;
    displayText.innerText += displayExpString;
  }
  return;
};

const checkAndInsert = (simpleExp, simpleExpDspl) => {
  if (expression === "0") {
    expression = simpleExp;
    displayText.innerText = simpleExpDspl;
    return true;
  }
  if (
    (expression[expression.length - 1] >= "0" &&
      expression[expression.length - 1] <= "9") ||
    expression[expression.length - 1] === ")"
  ) {
    expression += `*${simpleExp}`;
    displayText.innerText += `*${simpleExpDspl}`;
    return "changed";
  }
};

const insertOperator = (operator) => {
  // console.table(bracketInfo);
  if (isPreviousCharOperator()) {
    expression = expression.slice(0, -1) + operator;
    displayText.innerText = displayText.innerText.slice(0, -1) + operator;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  } else if (isAvoidable[expression[expression.length - 1]]) {
    if (operator === "-" && expression[expression.length - 1] === "(") {
      expression += operator;
      displayText.innerText += operator;
      return;
    } else {
      return;
    }
  } else {
    expression += operator;
    displayText.innerText += operator;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  }
  return;
};

additionButton.addEventListener("click", () => insertOperator("+"));

devisionButton.addEventListener("click", () => insertOperator("/"));

multiplyButton.addEventListener("click", () => insertOperator("*"));

moduloButton.addEventListener("click", () => insertOperator("%"));

subtractButton.addEventListener("click", () => insertOperator("-"));

expoButton.addEventListener("click", () => {
  if (expression[expression.length - 1] === "(") {
    return;
  }
  expression += "**";
  displayText.innerText += "^";
});

squareButton.addEventListener("click", () => {
  if (expression[expression.length - 1] === "(") {
    return;
  }
  expression += "**2";
  displayText.innerText += "^2";
});

tensExpoButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("10**", "10^")
);

sqrtButton.addEventListener("click", () => {
  openingBracketInfoHandler();
  const isAlreadyInserted = checkAndInsert("Math.sqrt(", "√(");
  if (!isAlreadyInserted) {
    expression += "Math.sqrt(";
    displayText.innerText += "√(";
  }
  currentDecimalInitIndexes.expIndx = expression.length;
  currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  return;
});

factorialButton.addEventListener("click", () => {
  // console.log(currentDecimalInitIndexes);
  let lastChar = expression[expression.length - 1];
  if (lastChar === "^") return;
  expression =
    expression.substring(0, currentDecimalInitIndexes.expIndx) +
    `fact(${expression.substring(currentDecimalInitIndexes.expIndx)})`;
  displayText.innerText += "!";
  return;
});

inverseButton.addEventListener("click", () => {
  // console.log(currentDecimalInitIndexes);
  let lastChar = expression[expression.length - 1];
  if (lastChar === "^") return;
  expression =
    expression.substring(0, currentDecimalInitIndexes.expIndx) +
    `inverse(${expression.substring(currentDecimalInitIndexes.expIndx)})`;
  displayText.innerText =
    displayText.innerText.substring(0, currentDecimalInitIndexes.dspexpIndx) +
    `(1/${displayText.innerText.substring(
      currentDecimalInitIndexes.dspexpIndx
    )})`;
});

//logarethmic function
naturalLogButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.log(", "ln(")
);

logTenButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.log10(", "log(")
);

//trigonmetric function
sinButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.sin(", "sin(")
);

cosButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.cos(", "cos(")
);

tanButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.tan(", "tan(")
);

// Degree to Radian and vice-versa even listner

degToRadInput.addEventListener("input", (e) => {
  if (e.target.value !== "") {
    radToDegInput.setAttribute("value", `${Calc.degToRad(e.target.value)}`);
    return;
  } else {
    radToDegInput.setAttribute("value", "");
  }
});

// equal -sign means evaluate equation button eventlistner
evaluateButton.addEventListener("click", () => {
  if (expression === "0") {
    return;
  }
  if (bracketInfo.isBracketsOpen) {
    for (let i = 0; i < bracketInfo.openingCounter; i++) {
      expression += ")";
      displayText.innerText += ")";
    }
  }
  const result = Calc.evaluateArithmetiExp(expression);
  if (result.__proto__ === String.prototype) {
    alert(result + " : " + displayText.innerText);
  } else {
    resultDisplay.innerHTML = `<span class="exp">${displayText.innerText}</span><span class="equal-sign">=</span><span class="output">${result}</span>`;
    expression = result.toString();
    displayText.innerText = expression;
    bracketInfo.isBracketsOpen = false;
    bracketInfo.openingCounter = 0;
    currentDecimalInitIndexes.expIndx = 0;
    currentDecimalInitIndexes.dspexpIndx = 0;
    // for adding focus style around display
    document.querySelector(".expression-cls").classList.add("highlight");
    setTimeout(() => {
      document
        .querySelector(".expression-cls.highlight")
        .classList.remove("highlight");
    }, 1500);
  }
});
